# Final Lo Visto

Práctica final del curso 2020/21

**#Datos**
 
- Nombre: Daniel Álvarez Bustos

- Titulación: ITT(tecnologías telecomunicación)  

- Usuario de laboratorio: alvarezb

- Correo universidad:d.alvarezb.2017@alumnos.urjc.es

- Despliegue (url):http://alvarezb.pythonanywhere.com/lovisto/

- Video básico (url): https://youtu.be/HHtR-5YmSms

- Video parte opcional(url): https://youtu.be/KhYaNxf1PCw

**#Cuenta Admin Site**
usuario/contraseña: admin/admin

**#Cuentas usuarios**
usuario/contraseña: daniel/daniel

**#Resumen parte obligatoria**
La práctica final de la asignatura consiste en la creación de una aplicación web, llamada “LoVisto”, En ella podrás gestionar las aportaciones que hacen los usuarios,que constaran de un titulo y URL.Cuando un usuario se meta en una aportación podrá enviar comentarios,modificar la url,comentario,titulo,borrar la aportacion y comentario.También el usuario podrá poner el modo oscuro y normal en las páginas que quiera.

**#Lista partes opcionales**

- Nombre parte: Inclusión de un favicon del sitio

- Nombre parte: Inclusión de imágenes en los comentarios.

- Nombre parte: Recursos no reconocidos
