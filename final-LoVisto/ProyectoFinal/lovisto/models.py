from django.db import models

# Create your models here.
class Contenido(models.Model):
    title = models.CharField(max_length=64) #Titulo de la noticia
    url = models.TextField() #La URL
    user = models.TextField()
    date = models.DateTimeField('published')
    Comments = models.IntegerField() #Numero de comentarios
    votes_pos = models.IntegerField()
    votes_neg = models.IntegerField()

    def __str__(self):
        return self.clave + ":" + self.valor

class Comentario(models.Model):
    content = models.ForeignKey(Contenido,on_delete=models.CASCADE) #Cada comentario va al contenido
    info = models.TextField() #Comentario a poner
    imagen = models.TextField(default="No_url")
    title = models.CharField(max_length=64)
    date = models.DateTimeField('published')
    user = models.TextField()

    def __str__(self):
        return self.title + ":" + self.valor

class Voto(models.Model):
    content = models.ForeignKey(Contenido,on_delete=models.CASCADE)#Un voto va para el contenido
    user = models.TextField() #usuario que ha votado
    valor = models.TextField() #Positivo o negativo

    def __str__(self):
        return str(self.content) + ":" + self.user





