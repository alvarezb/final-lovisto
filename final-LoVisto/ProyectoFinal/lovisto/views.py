from .models import Contenido, Comentario, Voto
from django.contrib.auth import logout, authenticate, login
from django.shortcuts import redirect,render
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from bs4 import BeautifulSoup
import requests
from django.http import JsonResponse

import requests
# Create your views here.

def logout_view(request):
    logout(request)

def get_last_from_list_5(n, lista):
    lista = lista.objects.all()
    return lista.order_by('-id')[:n]

@csrf_exempt
def indexpage(request):

    modo = 0 #Por defecto claro
    if request.method == "POST":
        action = request.POST['action']
        if action == "Iniciar sesion":
            return redirect('/login')
        elif action == "Cerrar sesion":
            logout_view(request)
            return redirect('/lovisto')
        elif action == "añadir contenido":
                title = request.POST['title']
                url = request.POST['url']
                try:
                    c = Contenido.objects.get(title=title)
                    c.delete()
                except Contenido.DoesNotExist:
                    c = Contenido(title=title,
                                url=url,
                                user=request.user.username,
                                date=timezone.now(),
                                Comments=0,
                                votes_pos=0,
                                votes_neg=0)
                    c.save()
        elif action == "Pagina Usuario":
            return redirect('/lovisto/user/' + request.user.username)
        elif action == "Aportaciones totales":
            return redirect('/lovisto/resumenaportaciones')
        elif action == "Informacion":
            return redirect('/lovisto/informacion')
        elif action == "Modo oscuro":
            modo= 1
        elif action == "Modo claro":
            modo = 0

    content_list = Contenido.objects.all()
    content_list = list(reversed(content_list))
    content_list_apor = content_list[0:10]
    last3_apor = get_last_from_list_5(3,Contenido)
    aportaciones = get_last_from_list_5(5, Contenido)
    context = {
        'content_list_apor': content_list_apor,
        'content_list': content_list,
        'numeroapor': aportaciones,
        'last3_apor':last3_apor,
        'modo':modo
    }
    return render(request, 'lovisto/PaginaPrincipal.html', context)

def user_page(request,llave):
    modo = 0#Por defecto claro
    if request.method =="POST":
        action=request.POST['action']
        if request.POST['action'] == "Pagina principal":
            return redirect('/lovisto')
        elif action== "Cerrar sesion":
            logout_view(request)
            return redirect('/lovisto')
        elif action== "Aportaciones totales":
            return redirect('/lovisto/resumenaportaciones')
        elif action == "Informacion":
            return redirect('/lovisto/informacion')
        elif action == "Modo oscuro":
            modo = 1
        elif action == "Modo claro":
            modo = 0
    content_list = Contenido.objects.all()
    coment_list = Comentario.objects.all()
    vote_list = Voto.objects.all()
    last3_apor = get_last_from_list_5(3, Contenido)
    context = {
         'content_list': content_list,
         'coment_list': coment_list,
         'vote_list':vote_list,
         'last3_apor':last3_apor,
         'modo':modo
     }
    return render(request,'lovisto/PaginaUsuario.html',context)

def contributions_page(request):
    modo=0#Por defecto claro
    if request.method == "POST":
        action = request.POST['action']
        if request.POST['action'] == "Pagina principal":
            return redirect('/lovisto')
        elif action == "Cerrar sesion":
            logout_view(request)
            return redirect('/lovisto')
        elif action == "Pagina Usuario":
            return redirect('/lovisto/user/' + request.user.username)
        elif action== "Informacion":
            return redirect('/lovisto/informacion')
        elif action == "Modo oscuro":
            modo = 1
        elif action == "Modo claro":
            modo = 0

    content_list = list(reversed(Contenido.objects.all()))
    coment_list = Comentario.objects.all()
    vote_list = Voto.objects.all()
    last3_apor = get_last_from_list_5(3, Contenido)
    context = {
        'content_list': content_list,
        'coment_list': coment_list,
        'vote_list': vote_list,
        'last3_apor':last3_apor,
        'modo':modo
    }
    return render(request, 'lovisto/aportaciones.html', context)

def page_info(request):
    modo=0#Por defecto claro
    if request.method =="POST":
        action=request.POST['action']
        if request.POST['action'] == "Pagina principal":
            return redirect('/lovisto')
        elif action == "Cerrar sesion":
            logout_view(request)
            return redirect('/lovisto')
        elif action == "Pagina Usuario":
            return redirect('/lovisto/user/' + request.user.username)
        elif action == "Informacion":
            return redirect('/lovisto/informacion')
        elif action == "Aportaciones totales":
            return redirect('/lovisto/resumenaportaciones')
        elif action == "Modo oscuro":
            modo = 1
        elif action == "Modo claro":
            modo = 0

    last3_apor = get_last_from_list_5(3, Contenido)
    context = {
        'last3_apor': last3_apor,
        'modo':modo
    }
    return render(request,'lovisto/Paginainformacion.html',context)

@csrf_exempt
def page_resource(request,llave):
    modo=0 #Por defecto claro
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    if request.method == "POST":
        if request.POST['action'] == "Iniciar sesion":
            return redirect('/login')

        elif request.POST['action'] == "Cerrar sesion":
            logout_view(request)
            return redirect('/lovisto')

        elif request.POST['action'] == "Pagina principal":
            return redirect('/lovisto')

        elif request.POST['action'] == "Pagina Usuario":
            return redirect('/lovisto/user/<request.user.username>')

        elif request.POST['action'] == "Aportaciones totales":
            return redirect('/lovisto/resumenaportaciones')

        elif request.POST['action'] == "Informacion":
            return redirect('/lovisto/informacion')

        elif request.POST['action'] == "Enviar comentario":
            contenido = Contenido.objects.get(title=llave)
            contenido.Comments = contenido.Comments + 1
            c = Comentario(content=contenido,info=request.POST['info'],
                            imagen=request.POST['url'],
                            title = request.POST['titulo'],
                            date=timezone.now(),
                            user=request.user.username)
            c.save()
            contenido.save()

        elif request.POST['action'] == "LIKE":
            contenido = Contenido.objects.get(title=llave)
            contenido.votes_pos = contenido.votes_pos + 1
            contenido.save()
            voto = Voto(content=Contenido.objects.get(title=llave),
                        user=request.user.username,
                        valor="like")
            voto.save()

        elif request.POST['action'] == "DISLIKE":
            contenido = Contenido.objects.get(title=llave)
            contenido.votes_neg = contenido.votes_neg + 1
            contenido.save()
            voto = Voto(content=Contenido.objects.get(title=llave),
                       user=request.user.username,
                       valor="dislike")
            voto.save()

        elif request.POST['action'] == "Borrar voto":
            contenido = Contenido.objects.get(title=llave)
            votedelete = Voto.objects.get(content=Contenido.objects.get(title=llave),
                                       user=request.user.username)
            if votedelete.valor == "like":
                contenido.votes_pos = contenido.votes_pos - 1
            elif votedelete.valor == "dislike":
                contenido.votes_neg = contenido.votes_neg - 1
            contenido.save()
            votedelete.delete()

        elif request.POST['action'] == "Borrar comentario":
            contenido = Contenido.objects.get(title=llave)
            try:
                comentariodelete = Comentario.objects.get(content=Contenido.objects.get(title=llave),
                                                      title=request.POST['comentario'],
                                                      user=request.user.username)
                contenido.Comments = contenido.Comments - 1
                contenido.save()
                comentariodelete.delete()
            except Comentario.DoesNotExist:
                content = Contenido.objects.get(title=llave)
                context = {"content": content}
                return render(request, 'lovisto/comentarionoencontrado.html', context)

        elif request.POST['action'] == "Modificar url":
            try:
                c= Contenido.objects.get(title=llave)
                c.url=request.POST['valor']
                c.save()
            except Contenido.DoesNotExist:
                c=Contenido(title=llave, url=request.POST['valor'])
                c.save()

        elif request.POST['action'] == "Modificar titulo":
            contenido = Contenido.objects.get(title=llave)
            if contenido:
                contenido.title = request.POST['titulo']
                contenido.save()
                return redirect('/lovisto/'+ request.POST['titulo'])

        elif request.POST['action'] == "Borrar aportacion":
            try:
                c = Contenido.objects.get(title=llave)
                c.delete()
                return redirect('/lovisto')
            except Contenido.DoesNotExist:
                c = Contenido.objects.get(title=llave, url=valor)
                c.save()

        elif request.POST['action'] == "Cambiar comentario":
            return render(request,'lovisto/cambiarcomentario.html',{})

        elif request.POST['action'] == "Modificar comentario":
            try:
                c = Comentario.objects.get(title=request.POST['titulo'])
                if c:
                    c.info = request.POST['descripcion']
                    c.save()
                    return redirect('/lovisto/'+ llave)
            except Comentario.DoesNotExist:
                content = Contenido.objects.get(title=llave)
                context = {"content": content}
                return render(request, 'lovisto/comentarionoencontrado.html', context)

        elif request.POST['action'] == "Modo oscuro":
            modo = 1
        elif request.POST['action'] == "Modo claro":
            modo = 0
    try:
        content = Contenido.objects.get(title=llave)
        context = {"content": content}
    except Contenido.DoesNotExist:
        #XML
        if request.GET.get("format") == "xml/":
            contenido=Contenido.objects.all()
            context = {'content': contenido}
            return render(request, 'lovisto/template_xml.html', context)
        #JSON
        if request.GET.get("format") == "json/":
            content = Contenido.objects.filter().values()
            return JsonResponse({"content": list(content)})

        last3_apor = get_last_from_list_5(3, Contenido)
        context = {'last3_apor': last3_apor,
                   'modo':modo}
        return render(request, 'lovisto/noexisteaportacion.html', context)

    content = Contenido.objects.get(title=llave)
    coment_list = Comentario.objects.all()
    vote_list = Voto.objects.all()
    last3_apor = get_last_from_list_5(3, Contenido)
    context = {
        'content': content,
        'coment_list': coment_list,
        'vote_list': vote_list,
        'last3_apor': last3_apor,
        'modo':modo
    }
    #Recursos reconocidos
    content = Contenido.objects.get(title=llave)
    url = content.url
    headers = {"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36"}
    try:
        page = requests.get(url, headers=headers) #descargar el contenido de la página
        soup = BeautifulSoup(page.text, 'html.parser') #texto en formato HTML y el parser a utilizar

        if url.startswith('https://') or url.startswith('http://'):
            url = url.split('//', maxsplit=1)[1]

        if "www.reddit.com" in url:
            try:
                text = soup.find("p", class_="_1qeIAgB0cPwnLhDF9XSiJM").text#texto
                #print(text)
                title = soup.find("title").text#titulo
                #print(title)
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'texto': text,
                    'title': title,
                    'last3_apor': last3_apor,
                }
                return render(request, 'lovisto/Recursoreditt.html', context)
            except AttributeError:
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'last3_apor': last3_apor
                }
                return render(request, 'lovisto/Recursoreconocidoerror.html', context)

        elif "www.aemet.es" in url:
            try:
                print(url)
                title = soup.find("title").text #Busco el titulo
                #print(title)
                temp = soup.find("div", class_="no_wrap").text
                #print(temp)
                estado = soup.find_all('div',{'class','no_wrap'})
                #print(estado)
                temp2 = ((estado[0])['title'])
                #print(temp2)
                temp3 = ((estado[1])['title'])
                #print(temp3)
                temp4 = ((estado[2])['title'])
                #print(temp4)
                temp5 = ((estado[3])['title'])
                #print(temp5)

                informacion = soup.find_all('img')
                #print(informacion)
                info1 = ((informacion[12])['title'])
                #print(info1)
                info2 = ((informacion[13])['title'])
                #print(info2)
                info3 = ((informacion[14])['title'])
                #print(info3)
                info4 = ((informacion[15])['title'])
                #print(info4)
                info5 = ((informacion[16])['title'])
                #print(info5)
                info6 = ((informacion[17])['title'])
                #print(info6)

                fecha = soup.find("th", class_="borde_izq_dcha_fecha").text
                #print(fecha)
                maxmin = soup.find("td", class_="alinear_texto_centro no_wrap comunes").text
                #print(maxmin)
                probprecipitacion = soup.find("td", class_="nocomunes").text
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'title': title,
                    'temp': temp,
                    'temp2':temp2,
                    'temp3':temp3,
                    'temp4':temp4,
                    'temp5':temp5,
                    'info1':info1,
                    'info2':info2,
                    'info3':info3,
                    'info4':info4,
                    'info5':info5,
                    'info6':info6,
                    'fecha': fecha,
                    'maxmin': maxmin,
                    'probprecipitacion': probprecipitacion,
                    'last3_apor': last3_apor
                }
                return render(request, 'lovisto/Recursoaemet.html', context)
            except AttributeError:
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'last3_apor': last3_apor
                }
                return render(request, 'lovisto/Recursoreconocidoerror.html', context)

        elif "es.wikipedia.org" in url:
            try:
                images = soup.findAll('img')
                title = soup.find(id="firstHeading").text
                imagen = ((images[0])['src'])
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'title': title,
                    'imagen': imagen,
                    'last3_apor': last3_apor
                }
                return render(request, 'lovisto/Recursowikipedia.html', context)
            except AttributeError:
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'last3_apor': last3_apor
                }
                return render(request, 'lovisto/Recursoreconocidoerror.html', context)
        #Añadiendo otros recursos reconocidos
        elif "www.marca.com" in url:
            try:
                #print(url)
                title = soup.find("title").text  # Busco el titulo
                #print(title)
                text = soup.find("p", class_="ue-c-article__standfirst").text
                #print(text)
                images = soup.findAll('img')
                imagen = ((images[1])['src'])
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'title': title,
                    'imagen':imagen,
                    'text':text,
                    'last3_apor': last3_apor
                     }
                return render(request, 'lovisto/Recursomarca.html', context)
            except AttributeError:
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'last3_apor': last3_apor
                }
                return render(request, 'lovisto/Recursoreconocidoerror.html', context)

        elif "www.abc.es" in url:
            try:
                print(url)
                title = soup.find("title").text  # Busco el titulo
                #print(title)
                images = soup.findAll('img')
                #print(images[5])
                imagen = ((images[5])['src'])
                #print(imagen)
                text = soup.find("h2", class_="subtitulo").text
                #print(text)
                context={
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'title':title,
                    'imagen':imagen,
                    'text':text,
                    'last3_apor': last3_apor
                }
                return render(request, 'lovisto/Recursoabc.html', context)
            except AttributeError:
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'last3_apor': last3_apor,
                    'modo':modo
                }
                return render(request, 'lovisto/Recursoreconocidoerror.html', context)
        else:
            try:
                title = soup.find("title").text
                images = soup.findAll('img')
                imagen = ((images[1])['src'])
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'title': title,
                    'img': imagen,
                    'last3_apor': last3_apor,
                    'modo':modo
                }
                return render(request, 'lovisto/Otrosrecursosnoreconocidos.html', context)
            except AttributeError:
                context = {
                    'content': content,
                    'coment_list': coment_list,
                    'vote_list': vote_list,
                    'last3_apor': last3_apor
                }
            return render(request, 'lovisto/Recursoreconocidoerror.html', context)
    except:
        context = {
        'content': content,
        'coment_list': coment_list,
        'vote_list': vote_list,
        'last3_apor': last3_apor,
        'modo':modo
        }
    return render(request, 'lovisto/Recursoreconocidoerror.html', context)




