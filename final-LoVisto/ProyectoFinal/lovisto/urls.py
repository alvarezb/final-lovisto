from django.urls import path,include
from . import views

urlpatterns = [
    path('',views.indexpage,name='indexpage'),
    path('user/<str:llave>', views.user_page,name='user_page'),
    path('resumenaportaciones', views.contributions_page,name='contributions_page'),
    path('informacion', views.page_info,name='page_info'),
    path('<str:llave>',views.page_resource,name='page_resource'),
]